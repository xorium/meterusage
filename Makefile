init:
	cp -n .env.example .env

test:
	go test ./...

lint:
	golangci-lint run

run: init
	go run cmd/main.go