package main

import (
	"github.com/caarlos0/env/v6"
	"github.com/joho/godotenv"
	log "github.com/sirupsen/logrus"
	"gitlab.com/xorium/meterusage/internal/server"
)

func init() {
	_ = godotenv.Load()
}

func parseConfig(cfg *server.Config) error {
	if err := env.Parse(cfg); err != nil {
		return err
	}

	level, err := log.ParseLevel(cfg.LogLevel)
	if err != nil {
		return err
	}
	log.SetLevel(level)
	log.SetFormatter(&log.JSONFormatter{})
	log.SetReportCaller(true)

	return nil
}

func main() {
	cfg := server.Config{}
	if err := parseConfig(&cfg); err != nil {
		panic(err)
	}

	go server.StartServer(cfg)
	go server.StartGateway(cfg)

	select {}
}
