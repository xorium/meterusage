package repo

import (
	"context"
	"encoding/csv"
	"errors"
	"fmt"
	"io"
	"os"
	"strconv"
	"time"

	mu "gitlab.com/xorium/gengrpc/gen/go/meterusage/v1"
	"google.golang.org/protobuf/types/known/timestamppb"
)

var (
	ErrInvalidCSVRecordColumnsNum = fmt.Errorf("invalid csv record columns num")
	ErrInvalidCSVRecordTimestamp  = fmt.Errorf("invalid csv record timestamp")
	ErrInvalidCSVRecordValue      = fmt.Errorf("invalid csv record value")
)

const (
	recordSize         = 2
	recordValueBitSize = 32
)

// FileSystemCSV implements Repo by reading data from filesystem as CSV records.
type FileSystemCSV struct {
	pathToCSV string
	tsLayout  string
}

func NewFileSystemCSV(pathToFile string) (*FileSystemCSV, error) {
	if _, err := os.Stat(pathToFile); os.IsNotExist(err) {
		return nil, err
	}
	return &FileSystemCSV{
		pathToCSV: pathToFile,
		tsLayout:  time.RFC3339,
	}, nil
}

func (r *FileSystemCSV) SetTimestampLayout(l string) {
	r.tsLayout = l
}

func (r *FileSystemCSV) ReadValues(_ context.Context, offset, limit int) ([]*mu.MeterValue, error) {
	if offset < 0 {
		offset = 0
	}
	if limit <= 0 {
		limit = 1
	}
	// TODO: increase performance by making pool of file descriptors.
	file, err := os.Open(r.pathToCSV)
	if err != nil {
		return nil, err
	}
	defer func() { _ = file.Close() }()

	reader := csv.NewReader(file)
	// Skipping lines by offset and first title line.
	for i := 0; i <= offset; i++ {
		_, err := reader.Read()
		if err != nil {
			return nil, err
		}
	}

	result := make([]*mu.MeterValue, limit)
	for i := offset; i < offset+limit; i++ {
		record, err := reader.Read()
		if err != nil {
			// If file content has been read we need to slice result if it's length
			// is not multiple of limit.
			if errors.Is(err, io.EOF) {
				result = result[:i-offset]
			}
			return result, err
		}
		if len(record) != recordSize {
			return nil, ErrInvalidCSVRecordColumnsNum
		}
		ts, err := time.Parse(r.tsLayout, record[0])
		if err != nil {
			return nil, fmt.Errorf("parse error: %w", ErrInvalidCSVRecordTimestamp)
		}
		value, err := strconv.ParseFloat(record[1], recordValueBitSize)
		if err != nil {
			return nil, fmt.Errorf("parse error: %w", ErrInvalidCSVRecordValue)
		}
		result[i-offset] = &mu.MeterValue{
			Timestamp: timestamppb.New(ts),
			Value:     float32(value),
		}
	}

	return result, nil
}
