package repo

import (
	"context"
	"errors"
	"io"
	"io/ioutil"
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/suite"
)

const csvFilePath = "/tmp/meterusage_test.csv"

func TestNewFileSystemCSV(t *testing.T) {
	a := assert.New(t)
	repo, err := NewFileSystemCSV("__not_existing_file_path__")
	a.Nil(repo)
	a.NotNil(err)
}

type ReadingRecordsTestSuite struct {
	suite.Suite
	recordsSets map[string]string
}

func (suite *ReadingRecordsTestSuite) BeforeTest(suiteName, testName string) {
	var (
		csvData string
		ok      bool
	)
	if csvData, ok = suite.recordsSets[testName]; !ok {
		suite.FailNow(suiteName, "can't find data for test", testName)
	}
	err := ioutil.WriteFile(csvFilePath, []byte(csvData), 0600)
	if err != nil {
		suite.FailNow(err.Error())
	}
}

func (suite *ReadingRecordsTestSuite) SetupSuite() {
	suite.recordsSets = map[string]string{
		"TestCorrectData": `time,meterusage
2019-01-01 00:15:00,55.09
2019-01-01 00:30:00,54.64
2019-01-01 00:45:00,55.18
2019-01-01 01:00:00,56.03
2019-01-01 01:15:00,55.77
2019-01-01 01:30:00,55.45
2019-01-01 01:45:00,55.74
2019-01-01 02:00:00,55.8
2019-01-01 02:15:00,55.62
2019-01-01 02:30:00,55.45
2019-01-01 02:45:00,55.52
2019-01-01 03:00:00,56.0
2019-01-01 03:15:00,55.88
2019-01-01 03:30:00,59.57
2019-01-01 03:45:00,62.9
2019-01-01 04:00:00,62.54
2019-01-01 04:15:00,62.18
2019-01-01 04:30:00,62.86
2019-01-01 04:45:00,62.73
2019-01-01 05:00:00,62.84
2019-01-01 05:15:00,63.52
2019-01-01 05:30:00,78.1
2019-01-01 05:45:00,108.79
2019-01-01 06:00:00,111.19
2019-01-01 06:15:00,118.67
2019-01-01 06:30:00,135.37
2019-01-01 06:45:00,142.55
2019-01-01 07:00:00,145.97
2019-01-01 07:15:00,141.31
2019-01-01 07:30:00,133.58
2019-01-01 07:45:00,131.19
2019-01-01 08:00:00,133.22
2019-01-01 08:15:00,135.18
2019-01-01 08:30:00,134.71
2019-01-01 08:45:00,134.07
2019-01-01 09:00:00,134.11
2019-01-01 09:15:00,137.63
`,
		"TestInvalidTimestamp": `time,meterusage
2019-01-01 00:15:00,55.09
2019-01-01 00:30:00,54.64
2019-01-01 00:45:00,55.18
2019-01-01T08:15:00,135.18
2019-01-01 08:30:00,134.71
2019-01-01 08:45:00,134.07
2019-01-01 09:00:00,134.11
2019-01-01 09:15:00,137.63
`,

		"TestInvalidValue": `time,meterusage
2019-01-01 00:15:00,55.09
2019-01-01 00:30:00,54.64
2019-01-01 00:45:00,55.18
2019-01-01 08:15:00,135.18
2019-01-01 08:30:00,abcd
2019-01-01 08:45:00,134.07
2019-01-01 09:00:00,134.11
2019-01-01 09:15:00,137.63
`,

		"TestDifferentColumnsNum": `time,meterusage,value2
2019-01-01T00:15:00.000,55.09,11
2019-01-01T00:30:00.000,54.64,11
2019-01-01T00:45:00.000,55.18,11
2019-01-01T08:15:00.000,135.18,11
2019-01-01T08:30:00.000,134.71,11
2019-01-01T08:45:00.000,134.07,11
2019-01-01T09:00:00.000,134.11,11
2019-01-01T09:15:00.000,137.63,11
`,
	}
}

func (suite *ReadingRecordsTestSuite) TestCorrectData() {
	ctx := context.Background()
	r, err := NewFileSystemCSV(csvFilePath)
	suite.Nil(err)
	r.SetTimestampLayout("2006-01-02 15:04:05")

	result, err := r.ReadValues(ctx, 0, -123)
	suite.Nil(err)
	suite.Equal(1, len(result))
	suite.Equal(int64(1546301700), result[0].Timestamp.Seconds)
	suite.Equal(float32(55.09), result[0].Value)

	result, err = r.ReadValues(ctx, 3, 4)
	suite.Nil(err)
	suite.Equal(4, len(result))
	expected := []struct {
		Timestamp int64
		Value     float32
	}{
		{1546304400, 56.03},
		{1546305300, 55.77},
		{1546306200, 55.45},
		{1546307100, 55.74},
	}
	for i, expectedItem := range expected {
		suite.Equal(expectedItem.Timestamp, result[i].Timestamp.Seconds)
		suite.Equal(expectedItem.Value, result[i].Value)
	}

	_, err = r.ReadValues(ctx, 3000, 4)
	suite.True(errors.Is(err, io.EOF))

	result, err = r.ReadValues(ctx, -10, 10000)
	suite.True(errors.Is(err, io.EOF))
	suite.Equal(len(result), 37)
}

func (suite *ReadingRecordsTestSuite) TestInvalidTimestamp() {
	ctx := context.Background()
	r, err := NewFileSystemCSV(csvFilePath)
	suite.Nil(err)
	r.SetTimestampLayout("2006-01-02 15:04:05")

	_, err = r.ReadValues(ctx, 0, 10000)
	suite.True(errors.Is(err, ErrInvalidCSVRecordTimestamp))
}

func (suite *ReadingRecordsTestSuite) TestInvalidValue() {
	ctx := context.Background()
	r, err := NewFileSystemCSV(csvFilePath)
	suite.Nil(err)
	r.SetTimestampLayout("2006-01-02 15:04:05")

	_, err = r.ReadValues(ctx, 0, 10000)
	suite.True(errors.Is(err, ErrInvalidCSVRecordValue))
}

func (suite *ReadingRecordsTestSuite) TestDifferentColumnsNum() {
	ctx := context.Background()
	r, err := NewFileSystemCSV(csvFilePath)
	suite.Nil(err)
	r.SetTimestampLayout("2006-01-02 15:04:05")

	_, err = r.ReadValues(ctx, 0, 10000)
	suite.True(errors.Is(err, ErrInvalidCSVRecordColumnsNum))
}

func (suite *ReadingRecordsTestSuite) TearDownTestSuite() {
	_ = os.Remove(csvFilePath)
}

func TestReadingRecordsTestSuite(t *testing.T) {
	suite.Run(t, new(ReadingRecordsTestSuite))
}
