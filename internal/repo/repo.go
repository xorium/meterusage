package repo

import (
	"context"
	mu "gitlab.com/xorium/gengrpc/gen/go/meterusage/v1"
)

// Repo define methods of an abstract repository of meter values.
type Repo interface {
	// ReadValues read limit value beginning from offset.
	// If offset is bigger than whole number of value or result
	// contains the last value in repository then method must return io.EOF
	// as error.
	// If offset + limit is bigger than total values count, then remaining values
	// list must be returned with io.EOF as error.
	ReadValues(ctx context.Context, offset, limit int) ([]*mu.MeterValue, error)
	// SetTimestampLayout setting the layout (how to parse) of the timestamp column.
	// By default, using time.RFC3339.
	SetTimestampLayout(layout string)
}
