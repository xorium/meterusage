package server

import (
	"context"
	"errors"
	"fmt"
	"io"
	"strconv"
	"time"

	"gitlab.com/xorium/meterusage/pkg/prometheus"

	log "github.com/sirupsen/logrus"

	mu "gitlab.com/xorium/gengrpc/gen/go/meterusage/v1"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

const maxPageSize = 500

var (
	ErrInvalidPageToken = fmt.Errorf("invalid page token")
	ErrInvalidPageSize  = fmt.Errorf("invalid page size")
)

func validateListElectricityRequest(req *mu.ListElectricityRequest) error {
	if req.GetPageSize() <= 0 || req.GetPageSize() > maxPageSize {
		return ErrInvalidPageSize
	}
	if req.GetPageToken() == 0 {
		req.PageToken = 1
	} else if req.GetPageToken() < 0 {
		return ErrInvalidPageToken
	}
	return nil
}

func (s MeterusageServer) ListElectricityValues(
	ctx context.Context, req *mu.ListElectricityRequest) (
	resp *mu.ListElectricityResponse, err error,
) {
	startTime := time.Now()
	defer func() {
		prometheus.CollectMethodDuration(
			"ListElectricityValues", time.Since(startTime),
		)
		prometheus.CollectTotalRequestsCount("ListElectricityValues")
		if err != nil {
			prometheus.CollectTotalFailedRequestsCount("ListElectricityValues")
		}
	}()

	if err = validateListElectricityRequest(req); err != nil {
		log.Errorf("Invalid ListElectricityValues request: %v", err)
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	dbStartTime := time.Now()
	values, err := s.repo.ReadValues(ctx, int(req.GetPageToken()-1), int(req.GetPageSize()))
	prometheus.CollectDBRequestDuration(
		"ReadValues", strconv.Itoa(int(req.GetPageSize())), time.Since(dbStartTime),
	)

	var isValuesHaveEnded = false
	if err != nil {
		isValuesHaveEnded = errors.Is(err, io.EOF)
		if !isValuesHaveEnded {
			log.Errorf("ReadValues from repo error: %v", err)
			return nil, status.Error(codes.Internal, err.Error())
		}
		if isValuesHaveEnded && len(values) == 0 {
			return nil, status.Error(codes.NotFound, err.Error())
		}
	}

	nextPageToken := req.GetPageToken()
	if !isValuesHaveEnded {
		nextPageToken = req.GetPageToken() + int32(len(values))
	}
	return &mu.ListElectricityResponse{
		NextPageToken: nextPageToken,
		Values:        values,
	}, nil
}
