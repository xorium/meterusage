package server

import (
	"context"
	"io/ioutil"
	"net"
	"os"
	"testing"

	"github.com/stretchr/testify/suite"
	mu "gitlab.com/xorium/gengrpc/gen/go/meterusage/v1"
	"gitlab.com/xorium/meterusage/internal/repo"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

const (
	tmpFilePath = "/tmp/meterusage_server_test.csv"
	grpcAddr    = ":9001"
)

var testData = []byte(`time,meterusage
2019-01-01 00:15:00,55.09
2019-01-01 00:30:00,54.64
2019-01-01 00:45:00,55.18
2019-01-01 01:00:00,56.03
2019-01-01 01:15:00,55.77
2019-01-01 01:30:00,55.45
2019-01-01 01:45:00,55.74
2019-01-01 02:00:00,55.8
2019-01-01 02:15:00,55.62
2019-01-01 02:30:00,55.45
2019-01-01 02:45:00,55.52
2019-01-01 03:00:00,56.0
2019-01-01 03:15:00,55.88
2019-01-01 03:30:00,59.57
2019-01-01 03:45:00,62.9
2019-01-01 04:00:00,62.54
2019-01-01 04:15:00,62.18
2019-01-01 04:30:00,62.86
2019-01-01 04:45:00,62.73
2019-01-01 05:00:00,62.84
2019-01-01 05:15:00,63.52
2019-01-01 05:30:00,78.1
2019-01-01 05:45:00,108.79
2019-01-01 06:00:00,111.19
2019-01-01 06:15:00,118.67
2019-01-01 06:30:00,135.37
2019-01-01 06:45:00,142.55
2019-01-01 07:00:00,145.97
2019-01-01 07:15:00,141.31
2019-01-01 07:30:00,133.58
2019-01-01 07:45:00,131.19
`)

func createTestServer(r repo.Repo) (*grpc.Server, *MeterusageServer) {
	gs := grpc.NewServer()
	s := NewMeterusageServer(r)
	mu.RegisterMeterusageServer(gs, s)
	return gs, s
}

func startServer(gs *grpc.Server, addr string) {
	lis, err := net.Listen("tcp", addr)
	if err != nil {
		panic(err)
	}
	if err = gs.Serve(lis); err != nil {
		panic(err)
	}
}

func getClient(addr string) mu.MeterusageClient {
	conn, err := grpc.Dial(addr, grpc.WithInsecure(), grpc.WithBlock())
	if err != nil {
		panic(err)
	}
	return mu.NewMeterusageClient(conn)
}

type ServerTestSuite struct {
	suite.Suite
	cli mu.MeterusageClient
	gs  *grpc.Server
	srv *MeterusageServer
}

func (suite *ServerTestSuite) SetupSuite() {
	if err := ioutil.WriteFile(tmpFilePath, testData, 0600); err != nil {
		suite.FailNow(err.Error())
	}
	r, err := repo.NewFileSystemCSV(tmpFilePath)
	suite.Nil(err)
	r.SetTimestampLayout("2006-01-02 15:04:05")
	suite.gs, suite.srv = createTestServer(r)
	go startServer(suite.gs, grpcAddr)
	suite.cli = getClient(grpcAddr)
}

func (suite *ServerTestSuite) TearDownSuite() {
	suite.gs.Stop()
	_ = os.Remove(tmpFilePath)
}

func (suite *ServerTestSuite) TestIncorrectPageTokenValidation() {
	ctx := context.Background()
	_, err := suite.cli.ListElectricityValues(ctx, &mu.ListElectricityRequest{
		PageToken: -2,
		PageSize:  10,
	})
	suite.NotNil(err)
	suite.True(status.Code(err) == codes.InvalidArgument)

	_, err = suite.cli.ListElectricityValues(ctx, &mu.ListElectricityRequest{
		PageToken: -1,
		PageSize:  10,
	})
	suite.NotNil(err)
	suite.True(status.Code(err) == codes.InvalidArgument)
}

func (suite *ServerTestSuite) TestIncorrectPageSizeValidation() {
	ctx := context.Background()
	_, err := suite.cli.ListElectricityValues(ctx, &mu.ListElectricityRequest{
		PageToken: 1,
		PageSize:  0,
	})
	suite.NotNil(err)
	suite.True(status.Code(err) == codes.InvalidArgument)

	_, err = suite.cli.ListElectricityValues(ctx, &mu.ListElectricityRequest{
		PageToken: 0,
		PageSize:  maxPageSize + 1,
	})
	suite.NotNil(err)
	suite.True(status.Code(err) == codes.InvalidArgument)
}

func (suite *ServerTestSuite) TestListElectricityValues() {
	ctx := context.Background()
	resp, err := suite.cli.ListElectricityValues(ctx, &mu.ListElectricityRequest{
		PageToken: 1,
		PageSize:  3,
	})
	suite.Nil(err)
	suite.Equal(3, len(resp.Values), "there are not enough results")
	lastValue := resp.Values[2]

	resp, err = suite.cli.ListElectricityValues(ctx, &mu.ListElectricityRequest{
		PageToken: resp.NextPageToken,
		PageSize:  5,
	})
	suite.Nil(err)
	suite.Equal(5, len(resp.Values))
	// Current first value must not be previous last value.
	currFirstValue := resp.Values[0]
	suite.NotEqual(lastValue.Timestamp.Seconds, currFirstValue.Timestamp.Seconds)
	suite.NotEqual(lastValue.Value, currFirstValue.Value)

	resp, err = suite.cli.ListElectricityValues(ctx, &mu.ListElectricityRequest{
		PageToken: 10,
		PageSize:  50,
	})
	suite.Equal(int32(10), resp.NextPageToken)
	suite.True(len(resp.Values) > 0)
	suite.Nil(err)

	_, err = suite.cli.ListElectricityValues(ctx, &mu.ListElectricityRequest{
		PageToken: 100,
		PageSize:  50,
	})
	suite.NotNil(err)
	suite.True(status.Code(err) == codes.NotFound)
}

func TestMeterusageServerSuite(t *testing.T) {
	suite.Run(t, new(ServerTestSuite))
}
