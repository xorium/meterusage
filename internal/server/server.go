package server

import (
	"context"
	"net"
	"net/http"

	_ "embed"

	"github.com/grpc-ecosystem/grpc-gateway/v2/runtime"
	log "github.com/sirupsen/logrus"
	mu "gitlab.com/xorium/gengrpc/gen/go/meterusage/v1"
	"gitlab.com/xorium/meterusage/internal/repo"
	"google.golang.org/grpc"
)

type Config struct {
	Debug            bool   `env:"DEBUG" envDefault:"false"`
	LogLevel         string `env:"LOG_LEVEL" envDefault:"info"`
	PrometheusListen string `env:"PROMETHEUS_LISTEN" envDefault:":2222"`
	DBStr            string `env:"DB_STR" envDefault:"/tmp/meterusage.csv"`
	GRPCAddr         string `env:"GRPC_ADDR" envDefault:":9000"`
	GatewayAddr      string `env:"GATEWAY_ADDR" envDefault:":9443"`
}

type MeterusageServer struct {
	debug bool
	repo  repo.Repo
}

func NewMeterusageServer(r repo.Repo) *MeterusageServer {
	return &MeterusageServer{repo: r}
}

func StartServer(cfg Config) {
	meterRepo, err := repo.NewFileSystemCSV(cfg.DBStr)
	if err != nil {
		panic(err)
	}
	meterRepo.SetTimestampLayout("2006-01-02 15:04:05")

	gs := grpc.NewServer()
	s := NewMeterusageServer(meterRepo)
	s.debug = cfg.Debug
	mu.RegisterMeterusageServer(gs, s)

	lis, err := net.Listen("tcp", cfg.GRPCAddr)
	if err != nil {
		panic(err)
	}

	log.Info("gRPC server starting on" + cfg.GRPCAddr)
	if err = gs.Serve(lis); err != nil {
		panic(err)
	}
}

//go:embed static/index.html
var staticPage string

func returnStatic(w http.ResponseWriter, _ *http.Request, _ map[string]string) {
	w.WriteHeader(200)
	_, err := w.Write([]byte(staticPage))
	if err != nil {
		w.WriteHeader(500)
		_, _ = w.Write([]byte(err.Error()))
	}
}

func StartGateway(cfg Config) {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	mux := runtime.NewServeMux()
	if err := mux.HandlePath("GET", "/web", returnStatic); err != nil {
		log.Warn("Can't register Web interface handler:", err)
	}
	opts := []grpc.DialOption{grpc.WithInsecure(), grpc.WithBlock()}
	err := mu.RegisterMeterusageHandlerFromEndpoint(ctx, mux, cfg.GRPCAddr, opts)
	if err != nil {
		panic(err)
	}

	log.Info("gRPC gateway starting on", cfg.GatewayAddr)
	if err := http.ListenAndServe(cfg.GatewayAddr, mux); err != nil {
		panic(err)
	}
}
