package prometheus

import (
	"time"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
)

var dbDurationRequestCollector = promauto.NewHistogramVec(
	prometheus.HistogramOpts{
		Name:    "db_req_duration_milliseconds",
		Help:    "Time taken to process the method",
		Buckets: []float64{10, 50, 100, 500, 1000, 2000, 4000, 8000, 16000},
	},
	[]string{"name", "offset"},
)

func CollectDBRequestDuration(reqName string, pageSize string, duration time.Duration) {
	dbDurationRequestCollector.WithLabelValues(reqName, pageSize).Observe(
		float64(duration / time.Millisecond),
	)
}
