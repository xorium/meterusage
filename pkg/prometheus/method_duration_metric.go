package prometheus

import (
	"time"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
)

var methodDurationCollector = promauto.NewHistogramVec(
	prometheus.HistogramOpts{
		Name:    "method_duration_milliseconds",
		Help:    "Time taken to process the method",
		Buckets: []float64{10, 50, 100, 500, 1000, 2000, 4000, 8000, 16000},
	},
	[]string{"name"},
)

func CollectMethodDuration(name string, duration time.Duration) {
	methodDurationCollector.WithLabelValues(name).Observe(
		float64(duration / time.Millisecond),
	)
}
