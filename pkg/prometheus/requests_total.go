package prometheus

import (
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
)

var totalRequestsCount = promauto.NewCounterVec(
	prometheus.CounterOpts{
		Name: "total_rpc_requests",
		Help: "Number of requests",
	},
	[]string{"method"},
)

func CollectTotalRequestsCount(method string) {
	totalRequestsCount.WithLabelValues(method).Inc()
}

var totalFailedRequestsCount = promauto.NewCounterVec(
	prometheus.CounterOpts{
		Name: "total_failed_rpc_requests",
		Help: "Number of failed requests",
	},
	[]string{"method"},
)

func CollectTotalFailedRequestsCount(method string) {
	totalFailedRequestsCount.WithLabelValues(method).Inc()
}
